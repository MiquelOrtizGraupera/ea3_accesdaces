import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class MainWithDataInputStream {

    public static void main(String[] args) throws IOException{
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        //exercici1();
        exercici4();
        //exercici2(lector);

    }


    private static void menu() throws IOException{
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);

        System.out.println("FITXER EA3");
        System.out.println("1- Crear arxiu");
        System.out.println("2- Modificar dades");
        System.out.println("3- Esborrar físicament un departament");
        System.out.println("4- Llegir fitxer binari");
        System.out.println("Si vols sortir escriu -1");
        int user = lector.nextInt();
                if(user == 1){
                    exercici1();
                }else if (user == 2){
                    exercici2(lector);
                }

    }

    //exercici 1 --> Crear fitxer binari
    private static void exercici1() throws IOException {
        File ficherBinari = new File("DepartamentBinari.dat");
        FileOutputStream fileOut = new FileOutputStream(ficherBinari,true);
        DataOutputStream escribirDataOut = new DataOutputStream(fileOut);

        String nomDepartament [] ={"Recursos Humans","Finances","Marqueting",

                "Development","Sales","Atenció Client","Internacional","Direcció","Secretaria","Neteja"};
        String localitatDepartament [] = {"Mollet","Barcelona","Sabadell","Cornellà"
                ,"Masnou","Terrasa","Badalona","Sant Adrià","Mataró","Girona"};

        int count =1;
        for(int i = 0; i< nomDepartament.length;i++){
            escribirDataOut.write(count);//idDepart
            escribirDataOut.writeUTF(nomDepartament[i]);
            escribirDataOut.writeUTF(localitatDepartament[i]);
            count++;
        }
        escribirDataOut.close();
        /*//Introduim totes les dades a una Arraylist

        List<Departament> llistaDep = new ArrayList<>();

        for(int i =0; i< nomDepartament.length; i++){
            int ind = i;
            int numDep = i*10;
            String nomDep = nomDepartament[i];
            String locDep = localitatDepartament[i];
            Departament nou = new Departament(ind,numDep,nomDep,locDep);
            llistaDep.add(nou);
        }
        //Comprovem que tots els elements son a la Arraylist

        for(int z=0;z< llistaDep.size();z++){
            System.out.println(llistaDep.get(z));
        }

        //Escriurem les dades desde l'ArrayList

        for (int i = 0; i < llistaDep.size(); i++){
            escribirDataOut.write(llistaDep.get(i).getIndex());
            escribirDataOut.write(llistaDep.get(i).getNumeroDep());
            escribirDataOut.writeUTF(llistaDep.get(i).getNomDep());
            escribirDataOut.writeUTF(llistaDep.get(i).getLocationDep());
        }
        escribirDataOut.close();//cerrar Stream*/
    }

    //exercici 2 --> Modificar Dades departament
    private static void exercici2(Scanner lector)throws IOException{
        File fitxer = new File("DepartamentBinari.dat");
        DataInputStream dataIs = new DataInputStream(new FileInputStream(fitxer));

        List<Departament> modifyList = new ArrayList<>();

        //Llegim fitxer e incorporem les dades a la Arraylist
        int index,numDep;
        String nomDep,locationDep;

        while(dataIs.available()>0){
            index=dataIs.readInt();
            numDep= dataIs.readInt();
            nomDep=dataIs.readUTF();
            locationDep=dataIs.readUTF();
            Departament nou = new Departament(index,numDep,nomDep,locationDep);
            modifyList.add(nou);
        }
        dataIs.close();

        //Dades per modificar els departaments
        System.out.println("Escriu el departament a Modificar");
        int indexDep = lector.nextInt();
        lector.nextLine();
        System.out.println("Cambia el nom del departament:");
        String nomDepChange = lector.nextLine();
        System.out.println("Cambia la localització:");
        String locationDepChange = lector.nextLine();

        //Imprimirem el departament amb les dades antigues a modificar
        for(int i=0;i< modifyList.size(); i++){
            if(i == indexDep){
                System.out.println("DADES ANTIGUES");
                System.out.println("Numero Departament: "+modifyList.get(i).getNumeroDep()+"\n"+
                "Nom del Departament: "+modifyList.get(i).getNomDep()+"\n"+
                "Localitat del Departament: "+modifyList.get(i).getLocationDep());
            }
        }
        //Modificarem les dades del departament i les imprimirem per pantalla
        for(int y =0;y< modifyList.size();y++){
            if(y==indexDep){
                modifyList.get(y).setNumeroDep(indexDep*10);
                modifyList.get(y).setNomDep(nomDepChange);
                modifyList.get(y).setLocationDep(locationDepChange);
                System.out.println("DADES NOVES");
                System.out.println("Numero Departament: "+modifyList.get(y).getNumeroDep()+"\n"+
                        "Nom del Departament: "+modifyList.get(y).getNomDep()+"\n"+
                        "Localitat del Departament: "+modifyList.get(y).getLocationDep());
            }
        }

        File fitxerModificat = new File("DepartamentBinari.dat");
        FileOutputStream outDades = new FileOutputStream(fitxerModificat,true);
        DataOutputStream inDades = new DataOutputStream(outDades);

        for(int z = 0; z < modifyList.size(); z++){
            inDades.write(modifyList.get(z).getIndex());
            inDades.write(modifyList.get(z).getNumeroDep());
            inDades.writeUTF(modifyList.get(z).getNomDep());
            inDades.writeUTF(modifyList.get(z).getLocationDep());
        }

        fitxer.deleteOnExit();
    }

    //Mètode per llegir el fitxer binari
    private static void exercici4() throws IOException{
        File ficher = new File("DepartamentBinari.dat");
        FileInputStream dataGo = new FileInputStream(ficher);
        DataInputStream dataIs = new DataInputStream(dataGo);


       int numDep;
       String nomDep,locDep;

        while(dataIs.available()>=0){
              numDep=dataIs.readInt();
              nomDep=dataIs.readUTF();
              locDep=dataIs.readUTF();
              System.out.println("NumDep: "+numDep+"\n"+"Nomdep: "+nomDep+"\n"
                      +"Location: "+locDep);
            }

    }
}
