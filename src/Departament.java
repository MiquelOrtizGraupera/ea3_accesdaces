public class Departament {
    public int index;
    public int numeroDep;
    public String nomDep;
    public String locationDep;

    @Override
    public String toString() {
        return "Departament{" +
                "index=" + index +
                ", numeroDep=" + numeroDep +
                ", nomDep='" + nomDep + '\'' +
                ", locationDep='" + locationDep + '\'' +
                '}';
    }


    public Departament(int index,int numeroDep, String nomDep, String locationDep) {
        this.index = index;
        this.numeroDep = numeroDep;
        this.nomDep = nomDep;
        this.locationDep = locationDep;
    }

    public int getIndex(){
        return index;
    }

    public void setIndex(int index){
        this.index = index;
    }
    public int getNumeroDep() {
        return numeroDep;
    }

    public void setNumeroDep(int numeroDep) {
        this.numeroDep = numeroDep;
    }

    public String getNomDep() {
        return nomDep;
    }

    public void setNomDep(String nomDep) {
        this.nomDep = nomDep;
    }

    public String getLocationDep() {
        return locationDep;
    }

    public void setLocationDep(String locationDep) {
        this.locationDep = locationDep;
    }
}
