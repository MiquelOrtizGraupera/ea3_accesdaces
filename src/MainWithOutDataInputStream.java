import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;


public class MainWithOutDataInputStream {
    public static void main(String[] args) throws IOException {
        ex1();
        ex2(1);
        ex3(9);
    }


    private static void ex1() throws IOException{
        File fitxer = new File("Departaments.dat");
        RandomAccessFile accesFitxer = new RandomAccessFile(fitxer,"rw");

        String departament [] = {"Recursos Humans","Finances","Marqueting",
                "Development","Sales","Atenció Client","Internacional","Direcció","Secretaria","Neteja"};
        String localitat [] = {"Mollet","Barcelona","Sabadell","Cornellà"
                ,"Masnou","Terrasa","Badalona","Sant Adrià","Mataró","Girona"};

        List<Departament> llista = new ArrayList<>();

        for(int i =0; i< departament.length; i++){
            int index = i;
            int numDep = i+10;
            String dep = departament[i];
            String loc = localitat[i];
            Departament nou = new Departament(i,numDep,dep,loc);
            llista.add(nou);
        }

        llista.forEach(System.out::println);

        StringBuffer nomDep = null;
        StringBuffer locDep = null;
        if(Files.exists(Path.of("Departaments.dat"))){
            for (int j=0; j< llista.size();j++){
                accesFitxer.writeInt(llista.get(j).getIndex());
                nomDep = new StringBuffer(llista.get(j).getNomDep());
                nomDep.setLength(10);
                accesFitxer.writeChars(nomDep.toString());
                accesFitxer.writeInt(llista.get(j).getNumeroDep());
                locDep = new StringBuffer(llista.get(j).getLocationDep());
                locDep.setLength(10);
                accesFitxer.writeChars(locDep.toString());
            }
        }
        accesFitxer.close();
    }

    private static void ex2(int index) throws IOException{
        //llista bytes = 4 + 4 + 20 + 20

        Scanner lector = new Scanner(System.in).useLocale(Locale.US);

        File fitxer = new File("Departaments.dat");
        RandomAccessFile accesFitxer = new RandomAccessFile(fitxer,"rw");

        llegirFitxer();

        long posicion = (index) * 48;
        accesFitxer.seek(posicion);
        if(accesFitxer.readInt() == index){
            System.out.println("Introdueix el nou numero de Departament");
            int nouNumDep = lector.nextInt();
            lector.nextLine();
            System.out.println("Introdueix el nou Nom");
            String nouNomDep = lector.nextLine();
            System.out.println("Introdueix la nova localitat");
            String nouLocDep = lector.nextLine();
            StringBuffer nomDep = new StringBuffer(nouNomDep);
            StringBuffer locDep = new StringBuffer(nouLocDep);
            accesFitxer.writeInt(nouNumDep);
            nomDep.setLength(10);
            locDep.setLength(10);
            accesFitxer.writeChars(nomDep.toString());
            accesFitxer.writeChars(locDep.toString());
            accesFitxer.close();
        }else{
            System.out.println("El departament no existeix");
        }
        accesFitxer.close();
        llegirFitxer();
    }
    private static void ex3(int index)throws IOException{
        llegirFitxer();
        File fitxer = new File("Departaments.dat");
        RandomAccessFile accesfitxer = new RandomAccessFile(fitxer,"rw");

        long position = index * 48;
        accesfitxer.seek(position+48);
        accesfitxer.setLength(accesfitxer.length()-48);
        accesfitxer.close();
        llegirFitxer();
    }
    //ex4
    private static void llegirFitxer() throws IOException{
        File fitxer = new File("Departaments.dat");
        RandomAccessFile accesFitxer = new RandomAccessFile(fitxer,"rw");
        char nom [] = new char[10],aux;
        char location [] = new char[10];
        //Visualitzar les dades actuals
        long position = 0;

        while(position != accesFitxer.length()) {
            accesFitxer.seek(position);
            int index = accesFitxer.readInt();
            for (int i = 0; i < nom.length; i++) {
                aux = accesFitxer.readChar();
                nom[i] = aux;
            }
            int dep = accesFitxer.readInt();
            for (int i = 0; i < location.length; i++) {
                aux = accesFitxer.readChar();
                location[i] = aux;
            }
            String nomFi = new String(nom);
            String locFi = new String(location);
            System.out.println("Index: " + index + " Nom: " + nomFi + " Dep: " + dep + " Location: " + locFi);
            position += 48;
        }
        accesFitxer.close();
    }
}
